# Wikipedia citation tools
# resolve.py
# Resolvers

from bs4 import BeautifulSoup as bs
from isbnlib import mask
import utils as u

async def doi0(doi):
    """Doi to title and False, or (False, False) if no title found."""
    url = "http://su8bj7jh4j.search.serialssolutions.com/?id=DOI"
    doi = doi.replace('/', '%2F')
    url = url.replace('DOI', doi)
    # print('URL: ' + url)
    try:
        html = await u.get(url)
        soup = bs(html, 'html5lib')
        citation_data = dict(zip([x.text.strip() for x in soup.select(".section .citation-data .span3")],
                                 [x.text.strip() for x in soup.select(".section .citation-data .span9")]))
        return citation_data['Journal:'], None
    except:
        return None, None

        
async def doi1(doi):
    """Doi to title and False, or (False, False) if no title found."""
    url = "http://search.crossref.org/dois?q=DOI"
    doi = doi.replace('/', '%2F')
    url = url.replace('DOI', doi)
    # print('URL: ' + url)
    try:
        content = await u.get(url)
        j = json.loads(content)
        if j:
            return bs(j[0]['fullCitation'], 'html5lib').select('i')[0].text, True
        else:
            return None, None
    except:
        return None, None


# WorldCat Advanced search lookup
async def isbn0(isbn):
    """Isbn to title and False, or (False, False) if no title found."""
    url = "https://www.worldcat.org/search?q=bn%3AISBN&qt=advanced&dblist=638"
    url = url.replace('ISBN', isbn)
    # print('URL: ' + url)
#    try:
    html = await u.get(url)
    # print(html)
    soup = bs(html, 'html5lib')
    title = soup.select(".name a strong")[0].text
    if not "ISBN" in title:
        publisher = soup.find("span", class_="itemPublisher").text.split(': ')[1].split(',')[0]
        if publisher:
            return title, publisher
        else:
            return title, None
    else:
        return None, None
#    except:
#        return None, None


# Bookfinder API
# http://www.bookfinder.com/search/?isbn=ISBN&st=xl&ac=qr
async def isbn1(isbn):
    """Isbn to title and False, or (False, False) if no title found."""
    url = "http://www.bookfinder.com/search/?isbn=ISBN&st=xl&ac=qr"
    url = url.replace('ISBN', isbn)
    # print('URL: ' + url)
    try:
        html = safeget(url)
        soup = bs(html, 'html5lib')
        title = soup.title.text.strip()
        title = title.split(' (')[0]
        if title and (('Search error' not in title) and ('ISBN' not in title) and ('BookFinder.com: Forbidden' not in title)):
            publisher = soup.find(itemprop="publisher").text.split(',')[0]
            return title.split(' by ')[0], publisher or None
        else:
            return None, None
    except:
        return None, None


# Isbnsearch API
# http://www.isbnsearch.org/isbn/ISBN
async def isbn2(isbn):
    """Isbn to title and False, or (False, False) if no title found."""
    url = "http://www.isbnsearch.org/isbn/ISBN"
    url = url.replace('ISBN', isbn)
    # print('URL: ' + url)
    try:
        html = await u.get(url)
        soup = bs(html, 'html5lib')
        title = soup.title.text
        publisher = [p.text.split(': ')[1] for p in soup.select(".bookinfo")[0].find_all('p') if "Publisher" in p.text][0]
        return title.split('|')[1].split('(')[0].strip(), publisher or None
    except:
        return None, None


# Open Library API
# http://openlibrary.org/api/books?bibkeys=ISBN:ISBN&details=true
async def isbn3(isbn):
    """Isbn to title and False, or (False, False) if no title found."""
    url = "http://openlibrary.org/api/books?bibkeys=ISBN:ISBN&details=true"
    url = url.replace('ISBN', isbn)
    # print('URL: ' + url)
    try:
        content = await u.get(url)
        j = json.loads(content.replace('var _OLBookInfo = ', '')[:-1])
        return j['ISBN:'+isbn]['details']['title'], j['ISBN:'+isbn]['details']['publishers']
    except:
        return None, None


# Amazon Advanced Search lookup
async def isbn4(isbn):
    """Isbn to title and False, or (False, False) if no title found."""
    url = "http://www.amazon.com/gp/search/ref=sr_adv_b/?search-alias=stripbooks&unfiltered=1&field-isbn=ISBN&field-dateop=During&sort=relevanceexprank&Adv-Srch-Books-Submit.x=22&Adv-Srch-Books-Submit.y=5"
    url = url.replace('ISBN', isbn)
    # print('URL: ' + url)
    try:
        html = await u.get(url)
        soup = bs(html, 'html5lib')
        title = soup.select('.s-access-detail-page h2')[0].text
        # API required for looking at the details of the book like this:
        #        publisher_page_link = soup.find("a", class_="a-link-normal a-text-normal")['href']
        #        publisher_page_soup = bs(await u.get(publisher_page_link).text, 'html5lib')
        return title, None
    except:
        return False, None


# Thomson Reuters Web of Knowledge Book Master List
# For checking if an ISBN is "scientific" or not
# http://wokinfo.com/cgi-bin/bkci/search.cgi
# Match: 978-0-415-59181-2
# Note: Probably needs minimum -s 5 to work!
async def is_academic(isbn):
    try:
        html = await u.post("http://wokinfo.com/cgi-bin/bkci/search.cgi", data={'search': mask(isbn), 'searchtype': 'and'})
        soup = bs(html, 'html5lib')
        title = soup.select("tbody")[1].select("td")[0].text.rstrip()
        if 'No matches to your query.' in title:
            return False
        else:
            return True
    except:
        return None


async def pmid0(pmid):
    """PMID to title and False, or (False, True) if no title found."""
    url = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db=pubmed&id=PMID"
    url = url.replace('PMID', pmid)
    # print('URL: ' + url)
    try:
        html = await u.get(url)
        soup = bs(html, 'html5lib')
        return soup.find('item', {'name':"Source"}).text, None
    except:
        return None, None

    
async def pmc0(pmc):
    """PMC to title and False, or (False, True) if no title found."""
    url = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db=pmc&id=PMC"
    url = url.replace('PMC', pmc)
    # print('URL: ' + url)
    try:
        html = await u.get(url)
        soup = bs(html, 'html5lib')
        return soup.find('item', {'name':"Source"}).text, None
    except:
        return None, None

    
async def arxiv0(arxiv):
    """Arxiv ID to title and False, or (False, True) if no title found."""
    url = "http://arxiv.org/abs/ARXIV"
    url = url.replace('ARXIV', arxiv)
    # print('URL: ' + url)
    try:
        html = await u.get(url)
        soup = bs(html, 'html5lib')
        doi = soup.find('meta', {'name':"citation_doi"})['content']
        resolved = doi0(doi)
        if resolved[0]:
            return resolved
        else:
            return doi1(doi)
    except:
        return None, None

    
async def arxiv1(arxiv):
    """Arxiv ID to title and False, or (False, True) if no title found."""
    url = "http://export.arxiv.org/api/query?id_list=ARXIV"
    url = url.replace('ARXIV', arxiv)
    # print('URL: ' + url)
    try:
        html = await u.get(url)
        soup = bs(html, 'html5lib')
        # print('Found doi:', soup.find('arxiv:doi').text)
        return doi0(soup.find('arxiv:doi').text), None
    except:
        return None, None

