Design goals for version 3:

 * restartable
 * efficient parallelism
 * one line at a time
 * keeps data
 * separate progress bar functions
 * no global variables ex. argv
 * no dynamic globals
 * check alan irwin
