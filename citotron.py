#!/usr/bin/python3
# Wikipedia citation tools
# citotron.py
# Main file

__version__ = '3.0.0'

from args import args
from bs4 import BeautifulSoup as bs
from collections import Counter as counter
from concurrent.futures import ThreadPoolExecutor
from functools import lru_cache
from itertools import repeat
from joblib import Parallel, delayed
from multiprocessing.dummy import Pool
from poliplot import *
from pprint import pprint as pprint
from requests import get, post
from resolve import *
from time import sleep
import csv, json, sys, re, os, resolve, asyncio
import settings as s
import utils as u

# ---- HELPERS ----


def print_fails():
    print("=" * 80)
    print("Fails by kind:")
    for k,v in kindsfails.items():
        print("    %s: %s" % (k,v))
    print("Fails by resolver:")
    for k,v in resolversfails.items():
        print("    %s: %s" % (k,v))
    print("Processed rows: %s" % total)
    print("Total fails: %s" % fails)
    print("Total successes: %s" % successes)
    print("Fail rate:", str((fails / total) * 100)[:4] + "%")


def progress(character):
    """Convenient dot machine type printer."""
    if args.debug:
        print(character, end='', flush=True)


def safeprint(*args):
    """Convert arguments to strings, False to "ERROR", print the result."""
    print(*[str(arg) if arg is not False else 'ERROR' for arg in args], sep="")

# This is like a global variable but instead it is a memoized function.
# The advantage is that it only initialises itself if it is necessary.
# You can call that a "lazy static variable".
# maxsize=1 is enough because the function has no arguments.
@lru_cache(maxsize=1) 
def academic_publishers():
    """Return a list of academic publishers read from file."""
    with open(os.path.join(s.datadir, s.scientific_publishers),
            mode='rt', encoding='utf-8') as f:
        publishers = [u.canonical(l.strip()) for l in f.readlines()]
    abbrevs = {'SOC': ' SOCIETY OF',
               'ACAD': 'ACADEMY OF ',
               'UNIV': 'UNIVERSITY OF',
               'NATL': 'NATIONAL',
               'PUBL': 'PUBLISHING',
               'LTD': 'LIMITED',
               'CORP': 'CORPORATION',
               'INC': 'INCORPORATED',
               'COLL': 'COLLEGE',
               'CO': ' COMPANY',
               'INST': 'INSTITUTE',
               'INT': 'INTERNATIONAL',
               'M I T': 'MIT',
               'MIT': 'MIT',
               'DIV': 'DIVISION',
               'LLC': 'LIMITED',
               'PUBS': 'PUBLISHERS'}
    for n, publisher in enumerate(publishers):
        for abbrev in abbrevs:
            r = re.compile('^abbrev\s|\sabbrev$|\sabbrev\s'.replace('abbrev', abbrev))
            if re.match(r, publisher):
                print('******')
                print(abbrev)
                print(publisher)
                print('++++++')
                publishers[n] = publisher.replace(abbrev, abbrevs[abbrev])
                print(publishers[n])
                print('......')
    print(publishers)
    return publishers


# ---- INIT ----

logfieldnames = "page_id", "page_title", "rev_id", "timestamp", "type", "id", "title", "title_token", "publisher", "publisher_token", "academic?"

# Enumerate generator so that we only read the file once
rowsio = csv.DictReader(open(os.path.join(s.datadir, args.inputfile), 'rt'),
                           delimiter="\t")
rows = list(csv.DictReader(open(os.path.join(s.datadir, args.inputfile), 'rt'),
                           delimiter="\t"))
rows = u.correct_isbn_rows(rows)
log = csv.DictWriter(open(s.logfile, 'w', encoding='utf-8'),
                     fieldnames=logfieldnames,
                     delimiter='|',
                     quotechar='"',
                     quoting=csv.QUOTE_MINIMAL)
log.writeheader()
out = csv.writer(open(args.outputfile, 'w', encoding='utf-8'),
                 delimiter='|',
                 quotechar='"',
                 quoting=csv.QUOTE_MINIMAL)

# ---- GLOBAL STATE ----

total, serial, success, fail = len(rows), u.tally(), u.tally(), u.tally()
kinds = [x[:-1] for x in dir(resolve) if '0' in x]
kindsfails = {k:v for k, v in zip(kinds, repeat(0))}

resolvers = dir(resolve)
resolversfails = {k:v for k, v in zip(resolvers, repeat(0))}

# ---- GENERAL FUNCTIONS ----

async def resolverow(row, n=0, serialnumber=False):
    '''
    WARNING: Recursive function!
    Resolves title and publisher of row based on id.
    - Calls resolver function resolve.<kind><n>.
    - Title found is success.
    - Title not found calls back this function with same args except n+1.
    - No resolver found is the exit condition.
    '''
 #   global total, kindsfails, resolversfails
#    serialnumber = serialnumber or serial()
    resolver = row['type'] + str(n)
    try:
        resolved = await globals()[resolver](row['id'])
        # OMG: The comma is necessary here (to make it a list, not a tuple):
        row['title'], row['publisher'] = *resolved,
        safeprint("-" * 80)
        # safeprint("[", str((serialnumber / total) * 100)[:4],"%] ",
        #            serialnumber, "/", total,
        #            " (", success(0), " successes / ", fail(0), " fails) ",
        #            str((fail(0) / serialnumber) * 100)[:4],"% error rate."
        #            " Cc: ", resolver, ":", "\n",
        #            "    ", resolved[0], ", [", resolved[1], "]")
        # sleep(args.sleep)
        # resolved[0] is None if title not found, otherwise string
        # resolved[1] is None if the row was not isbn,
        #                None if it was isbn and publisher was not found,
        #                str if it was isbn and publisher was found.
        if row['title']:
            print(row['title'])
#            success(1)
#            postprocess(row)
        else:
            # TODO refactor to closure(s)?
#            resolversfails[resolver] += 1
            await resolverow(row, n+1, serialnumber)
    except KeyError:
        pass
#        fail(1)
#        kindsfails[row['type']] += 1
#        postprocess(row)

def postprocess(row):
    '''
    Postprocessing tasks:
    0. There are already two new columns compared to input: title and publisher.
    1. Add columns: title_token and publisher_token. These are for comparison.
    2. Add column: academic?
    '''
    print("POSTPROCESS")
    # Check if title, publisher was found and if yes then postprocess it:
    row['title_token'] = u.expand(u.canonical(row['title'])) if row['title'] else None
    row['publisher_token'] = u.canonical(row['publisher']) if row['publisher'] else None
    # Everything except books are automatically considered academic:
    if row['type'] is not 'isbn':
        row['academic?'] = True
    # Books with no publisher found are neither academic nor non-academic:
    elif not row['publisher']:
        row['academic?'] = None
    # Books with publishers are academic if:
    # 1. the publisher is in the list of academic publishers OR
    # 2. listed in the Thompson-Reuters Master Book List.
    else:
        row['academic?'] = row['publisher'] in academic_publishers() or is_academic(row['publisher'])
    pprint(row)
    log.writerow(row)


# async def resolverows(rowsio=rowsio):
#     while True:
#         try:
#             print("Resolverow...")
#             await asyncio.wait([resolverow(next(rowsio))])
#         except StopIteration:
#             break


async def resolverows(rowsio):
    await asyncio.wait([resolverow(row) for row in rowsio])

#async def resolverows(rowsio):
#    async for row in rowsio:
#        await resolverow(row)


# ---- MISC. FUNCTIONS ----

def isbns_in_libthing(rows):
    '''
    Tells how many ISBNS of rows are in Library Thing.
    '''
    # List of all ISBNs in Library Thing:
    # http://www.librarything.com/feeds/AllLibraryThingISBNs.csv
    libthing = open(os.path.join(s.datadir,
                                 'AllLibraryThingISBNs.csv'), 'r').readlines()
    libthing = [x.rstrip() for x in libthing]
    isbns = [row['id'] for row in rows if row['type'] == 'isbn']
    hits = 0
    for isbn in isbns:
        if isbn in libthing:
            hits += 1
    print("We have %s ISBNS and only %s of them are in Library Thing." % (str(len(isbns)), str(hits)))
    return hits


def count_titles(titles):
    '''
    Accepts a list of titles, logging and returning their frequencies.
    '''
    freq = counter(titles).most_common()
    for k, v in freq:
        out.writerow([k, v])
    return freq


def count_types(rows):
    """Counts how many rows of each kind (isbn, doi, etc.) are in the input csv."""
    types = []
    for row in rows:
        types.append(row['type'])
    return counter(types).most_common()


def filter_kind(kind, rows):
    return [row for row in rows if row['type'] == kind]


def scisbn(rows):
    """Returns statistics about which ISBNs are scientific"""
    list_of_scientific_publishers = scientific_publishers()
    results = resolverows(rows)
    # Remove cases where we could not find the publisher:
    nr_all_results = len(results)
    results = [ x for x in results if x[1] ]
    nr_results_with_publishers = len(results)
    results = [ x for x in results if x[1] in list_of_scientific_publishers ]
    nr_results_from_scientific_publishers = len(results)
    nr_results_not_from_scientific_publishers = nr_results_with_publishers - nr_results_from_scientific_publishers
    return {'All books resolved': nr_all_results,
            'Books with known publishers': nr_results_with_publishers,
            'Books with scientific publishers': nr_results_from_scientific_publishers,
            'Books with non-scientific publishers': nr_results_not_from_scientific_publishers,
            'Percent of books with scientific publishers from all books with known publishers': nr_results_from_scientific_publishers / float(nr_results_with_publishers)}

# ---- CALLS ----

if args.kind != 'all':
    rows = filter_kind(args.kind, rows)
    total = len(rows)

m = args.mode
if m == "count":
    pprint(count_titles(resolverows(rows)))
    print_fails()
    plot_occurrences(args.outputfile, args.discriminant)
elif m == "resolve":
    resolverows(rows)
    print_fails()
elif m == "types":
    pprint(count_types(rows))
elif m == "scisbn":
    pprint(scisbn(filter_kind('isbn', rows)))
elif m == "journaltitles":
    isbnrows = filter_kind('isbn', rows)
    otherrows = [ row for row in rows if row not in isbnrows ]
    pprint(count_titles(resolverows(otherrows)))
    print_fails()
elif m == "experiment":
    try:
        loop = asyncio.get_event_loop()
        print("Event loop running!")
        loop.run_until_complete(resolverows(rowsio))
        print("Event loop completed!")
    finally:
        loop.close()
        print("Event loop closed!")
else:
    print("mode was: '" + str(m) + "'")
    print("="*80)
    print("You have to choose a supported mode!")
    print("Try to run the script without arguments to get a help message.")
